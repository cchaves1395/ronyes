<!DOCTYPE html>
<html lang="es">
    
     <?php
        if (isset($this->session->userdata['login'])) {
            
        } else {
            redirect("login");
        }
    ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Ronyes, complejo deportivo</title> 

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>/files/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>files/css/style.css" rel="stylesheet">


</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top nav-custom" role="navigation">

        

        <a class="btn btn-agregar btn-volver-principal" href="<?php echo base_url(); ?>principal">
      <span class="glyphicon glyphicon-home"></span>
    </a>  
        <!-- /.container -->
    </nav>

  
    
    <div class="container registro-content">
    <h1>Registro de Equipos</h1>
    <h3>Por favor complete el siguiente formulario</h3> 
    <?php echo form_open('registroEquipo/registrar'); ?>
    <center>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">

            <input type="text" name="nombre" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" value="<?php echo set_value('nombre'); ?>" size="50" placeholder="Nombre del equipo">
            <input type="text" name="encargado" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" value="<?php echo set_value('encargado'); ?>" size="50" placeholder="Nombre del encargado">
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 registro-content-campos">
            <input type="number" name="cantIntegrantes" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" value="<?php echo set_value('cantIntegrantes'); ?>" size="50" placeholder="Cantidad de Integrantes">
            <input type="number" name="contacto" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" value="<?php echo set_value('contacto'); ?>" size="50" placeholder="Número telefónico del encargado">
      </div>
    </center>
    <button href="" class="btn btn-agregar" >Registrar</button>
    <a class="btn btn-salir-standard" href=" <?php echo base_url() ?>principalEquipos">Cancelar</a>  

</div> 



     <footer>
       <div class="leyenda-footer">
           <p>Complejo Deportivo Ronyes </p>
           <p>Ubicados 1km al este de la agencia ICE, Esparza.
           </p>
       </div>
       <div class="icons-container-footer">
           <a href="">
            <img src="<?php echo base_url() ?>/files/img/facebook.png" alt="" class="">
            Siguenos!
            </a>
           <a href="">
            <img src="./img/maps.png" alt="" class="">
            Visitanos!
            </a>            
       </div>
       <p>Desarrollado por: Wildness Developers</p>
   </footer>

    <!-- jQuery -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript REQUERIDO-->
    
    <script src="js/scrolling-nav.js"></script>

</body>

</html>