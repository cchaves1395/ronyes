<!DOCTYPE html>
<html lang="es">
    
    <?php
        if (isset($this->session->userdata['login']) == true) {
            
        } else {
            redirect("login");
        }
    ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Ronyes, complejo deportivo</title> 

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>/files/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>files/css/style.css" rel="stylesheet">

    
    
   

</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top nav-custom" role="navigation">

        
        <!-- /.container -->
    </nav>

      <div class="principal-content">
        <div class="login-title">
        <h1>Complejo Deportivo Ronyes</h1>          
        </div>
        <div class="opciones-container">
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <a href="<?php echo base_url();?>reservacion" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-principal">Reservación
            <img src="<?php echo base_url(); ?>files/img/principal/canchaicon.png">
          </a>
          <a href=" <?php echo base_url(); ?>principalretos" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-principal">Retos
            <img src="<?php echo base_url(); ?>files/img/principal/retos.png">
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <a href="<?php echo base_url();?>principalEquipos" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-principal">Equipos
          <img src="<?php echo base_url(); ?>files/img/principal/equipos.png">
          </a>
            <form action="">
          <a href="<?php echo base_url(); ?>Login/cerrar" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-salir">Salir
            <img src="<?php echo base_url(); ?>files/img/principal/exit.png">
          </a>
            </form>
        </div>
        </div>
        
        

        
      </div>  

   <footer>
       <div class="leyenda-footer">
           <p>Complejo Deportivo Ronyes </p>
           <p>Ubicados 1km al este de la agencia ICE, Esparza.
           </p>
       </div>
       <div class="icons-container-footer">
           <a href="">
            <img src="<?php base_url() ?>/files/img/facebook.png" alt="" class="">
            Siguenos!
            </a>
           <a href="">
            <img src="./img/maps.png" alt="" class="">
            Visitanos!
            </a>            
       </div>
       <p>Desarrollado por: Wildness Developers</p>
   </footer>

    <!-- jQuery -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript REQUERIDO-->
    
    <script src="js/scrolling-nav.js"></script>

</body>

</html>
