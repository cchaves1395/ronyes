<!DOCTYPE html>
<html lang="es">
    
     <?php
        if (isset($this->session->userdata['login'])) {
            
        } else {
            redirect("login");
        }
    ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Ronyes, complejo deportivo</title> 

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>/files/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>files/css/style.css" rel="stylesheet">


</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top nav-custom" role="navigation">

        

        <a class="btn btn-agregar btn-volver-principal" href="<?php echo base_url(); ?>principal">
      <span class="glyphicon glyphicon-home"></span>
    </a>  
        <!-- /.container -->
    </nav>
    
<div class="container registro-content">
    <h2>Reservación de Cancha <?php echo $idCancha ?><br>Fecha: <?php echo $fecha?> Hora: <?php echo $hora?></h2>
    
             
    <?php echo form_open('modificarReservacion/guardar'); ?>
    
        
        <input hidden="true" readonly="true" type="text" name="fecha" value="<?php echo $fecha?><?php echo set_value('fecha'); ?>" class="">
        <input hidden="true" readonly="true" type="text" name="hora" value="<?php echo $hora?>" class="camposEstandar">
        <input hidden="true" type="text" name="idCancha" value="<?php echo $idCancha?>" class="camposEstandar" size="50">
        <input hidden="true" type="text" name="idReservacion" value="<?php echo $idReservacion?>" class="camposEstandar">

        
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <h3 class="col-x-12 col-sm-12 col-md-12 col-lg-12">Equipo 1</h3>
          <div class="col-x-12 col-sm-12 col-md-12 col-lg-12">
            <label class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Nombre equipo o encargado:</label>
            <input type="text" name="equipo1" value="<?php echo $equipo1?>" class="campos col-lg-12" ></div>
          <div class="col-lg-12"><label class="col-xs-12 col-sm-12 col-md-12">Telefono:<br> </label>

            <input type="number" name="contacto1" value="<?php if($contacto1==0)
            echo '';
            else echo $contacto1?>" class="campos col-lg-12"></div>
        </div>



       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> 
          <div class="col-lg-12"> 
          <h3 class="col-x-12 col-sm-12 col-md-12 col-lg-12">Equipo 2</h3>        
            <label class="col-xs-12 col-sm-12 col-md-12">Nombre equipo o encargado:</label>
            <input type="text" name="equipo2" value="<?php echo $equipo2?>" class="campos col-lg-12">
          </div>
          <div class="col-lg-12">
            <label class="col-xs-12 col-sm-12 col-md-12">Telefono:</label>            
            <input type="number" name="contacto2" value="<?php if($contacto2==0)
            echo '';
            else echo $contacto1?>" class="campos col-lg-12" size="50">
          </div>    
        </div>
            
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">    
    
      <label>Reto:</label><input type="checkbox" class="" name="reto" id="reto" style="width:50px; height:30px;
      " <?php if($reto=='si')echo 'checked' ?>>
    
      
      <label>Arbitro:</label>
      <input type="checkbox" class="" name="arbitro" id="arbitro" style="width:50px; height:30px;"
      <?php if ($arbitro=='si')
      echo 'checked'?>>
      <br>
      <br>
      <br>
      <button class="btn btn-agregar" href="" >Guardar</button>
      <button class="btn btn-success" href="" >Eliminar</button>
      <a class="btn btn-salir-standard" href="">Cancelar</a>  
      </div>
          
    
        
    

</div>    
    



     <footer>
       <div class="leyenda-footer">
           <p>Complejo Deportivo Ronyes </p>
           <p>Ubicados 1km al este de la agencia ICE, Esparza.
           </p>
       </div>
       <div class="icons-container-footer">
           <a href="">
            <img src="<?php echo base_url() ?>/files/img/facebook.png" alt="" class="">
            Siguenos!
            </a>
           <a href="">
            <img src="./img/maps.png" alt="" class="">
            Visitanos!
            </a>            
       </div>
       <p>Desarrollado por: Wildness Developers</p>
   </footer>

    <!-- jQuery -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript REQUERIDO-->
    
    <script src="js/scrolling-nav.js"></script>

</body>

</html>