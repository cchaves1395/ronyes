<!DOCTYPE html>
<html lang="es">
    
     <?php
        if (isset($this->session->userdata['login'])) {
            
        } else {
            redirect("login");
        }
    ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Ronyes, complejo deportivo</title> 

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>/files/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>files/css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top nav-custom" role="navigation">

        
        <a class="btn btn-agregar btn-volver-principal" href="<?php echo base_url(); ?>principal">
      <span class="glyphicon glyphicon-home"></span>
    </a>  
        <!-- /.container -->
    </nav>
    <div class="container principal-equipos">
      <h1>Reservación Canchas</h1>
      <label>Fecha:</label>
      <input class="fechaConsulta camposEstandar" type="date" name="" value="<?php echo mdate('%Y-%m-%d',strtotime($reservaciones[0]->fecha)); ?>"min="<?php echo 
      mdate('%Y-%m-%d', time());
      ?>">
      <label>Cancha:</label>
      <select class="idCancha camposEstandar">  
      <option>[Seleccione una cancha]</option>      
        <option <?php if ($reservaciones[0]->idCancha =='1'){
          echo 'selected';
          }else{
            echo '';
            } ;?> value="1">Cancha 1</option>
        <option <?php if ($reservaciones[0]->idCancha =='2'){
          echo 'selected';
          }else{
            echo '';
            } ;?> value="2">Cancha 2</option>
      </select>
      <h3>
    Reservaciones del: 
           <?php echo $reservaciones[0]->fecha; ?>
           De la cancha: <?php echo $reservaciones[0]->idCancha; ?>
          
       </h3>

       <!-- <h2>Controller hora <?php echo $horaParaView ; ?></h2>-->
        
      <table class="table table-stripped tabla-equipos">
        <thead>
        <tr>
          <th>Hora</th>
          <th>Estado</th>          
        </tr>          
        </thead>
        <tbody>
        <?php           

           foreach ($reservaciones as $row) {// inicia foreach
             
       ?>
        <tr>
          <td>
            <button class="btn btn-info"onclick="enviarObjeto('<?php echo $row->idReservacion;?>','<?php echo $row->fecha;?>','<?php echo $row->hora;?>','<?php echo $row->idCancha;?>','<?php echo $row->equipo1;?>','<?php echo $row->equipo2;?>','<?php echo $row->contacto1;?>','<?php echo $row->contacto2;?>','<?php echo $row->arbitro;?>','<?php echo $row->reto;?>')">
              <?php echo $row->hora;?>
            </td>   
            </button>
           
          <td>
            <?php
            if($row->reto =='si'){
              if($row->equipo2 =='' || $row->equipo1 ==''){
                echo "Pendiente";
              }else{
                echo"Completa";
              }
            }else if($row->equipo1!='' || $row->equipo2 !=''){
                echo "Completa";
              
            } else {
              echo "Libre";
            }
             ?>
            
          </td>
        <tr>
          <?php 
         }//cierre de foreach
         ?>
        </tbody>
      </table>
      <div class="botones-equipos-container">        
        <a href=" <?php echo base_url(); ?>principal" class="btn btn-salir-standard">Volver a Principal</a>
        <a href="<?php echo base_url(); ?>registroEquipo" class="btn btn-agregar">Añadir</a>        
      </div>
    </div> 

     <footer>
       <div class="leyenda-footer">
           <p>Complejo Deportivo Ronyes </p>
           <p>Ubicados 1km al este de la agencia ICE, Esparza.
           </p>
       </div>
       <div class="icons-container-footer">
           <a href="">
            <img src="<?php echo base_url() ?>/files/img/facebook.png" alt="" class="">
            Siguenos!
            </a>
           <a href="">
            <img src="./img/maps.png" alt="" class="">
            Visitanos!
            </a>            
       </div>
       <p>Desarrollado por: Wildness Developers</p>
   </footer>

    <!-- jQuery -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript REQUERIDO-->
    
    <script src="js/scrolling-nav.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){        
        var fecha;
        var idCancha;
        $('.fechaConsulta').change(function(){          
          fecha = $(this).val();          
          idCancha = $('.idCancha').val(); 
          window.location = "<?php echo base_url() ?>Reservacion/consultarConFecha/"+fecha+"/"+idCancha;          

        });


        $(".idCancha").on('change',  function () {
        fecha = $('.fechaConsulta').val();    
        idCancha=$(this).val();
            window.location = "<?php echo base_url() ?>Reservacion/consultarConFecha/"+fecha+"/"+idCancha;
        });




        
      });// fin del js
    </script>

</body>

<script type="text/javascript">
  
  function enviarObjeto(idReservacion, fecha, hora, idCancha, equipo1, equipo2, contacto1, contacto2, arbitro, reto){                 
       var fechaReservacion = fecha;
       //alert(fechaReservacion);

       $.ajax({
        type:'POST',
        data:{fechaReservacion: fechaReservacion, idReservacion: idReservacion, hora: hora, idCancha: idCancha, equipo1:equipo1, equipo2:equipo2, contacto1:contacto1, contacto2:contacto2, arbitro:arbitro, reto:reto},
        url:'<?php echo base_url();?>modificarreservacion/',
        success: function(result){
          //alert(result);
        $('body').html(result);
        }
       });
        }

</script>

</html>