<!DOCTYPE html>
<html lang="es">
    
     <?php
        if (isset($this->session->userdata['login'])) {
            redirect("principal");
        } 
    ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Ronyes, complejo deportivo</title> 

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>/files/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>/files/css/style.css" rel="stylesheet">
    
   

</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top nav-custom" role="navigation">

    
        <!-- /.container -->
    </nav>

      <div class="container login-content">
        <div class="login-title">
        <h1>Inicio de Sesión</h1>          
        </div>
        
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 imagen-container">
        <center>
        <img src="./files/img/logo.png" class="img img-responsive">
        </center>
        
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 campos-container">
          
            <?php echo form_open('login/procesar'); ?>
            <input type="text" name="cedula" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" value="<?php echo set_value('cedula'); ?>" size="50" placeholder="Número de cédula">
            <input type="password" name="contraseña" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" value="<?php echo set_value('contraseña'); ?>" size="50" placeholder="Contraseña">
            <button type="submit" name="Ingresar" class="btn btn-info btn-login">Ingresar</button>

        </div>

        
      </div>  
      <a href="<?php echo base_url('registroEquipo/') ?>">Link</a>

   <footer>
       <div class="leyenda-footer">
           <p>Complejo Deportivo Ronyes </p>
           <p>Ubicados 1km al este de la agencia ICE, Esparza.
           </p>
       </div>
       <div class="icons-container-footer">
           <a href="">
            <img src="./img/facebook.png" alt="" class="">
            Siguenos!
            </a>

           <a href="">
            <img src="./img/maps.png" alt="" class="">
            Visitanos!
            </a>            
       </div>
       <p>Desarrollado por: Wildness Developers</p>
   </footer>

    <!-- jQuery -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript REQUERIDO-->
    
    <script src="js/scrolling-nav.js"></script>

</body>

</html>
