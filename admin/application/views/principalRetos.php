<!DOCTYPE html>
<html lang="es">
    
     <?php
        if (isset($this->session->userdata['login'])) {
            
        } else {
            redirect("login");
        }
    ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Ronyes, complejo deportivo</title> 

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>/files/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>files/css/style.css" rel="stylesheet">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top nav-custom" role="navigation">

        <a class="btn btn-agregar btn-volver-principal" href="<?php echo base_url(); ?>principal">
      <span class="glyphicon glyphicon-home"></span>
    </a>
        <!-- /.container -->
    </nav>
    <div class="container principal-equipos">
      <h1>Equipos para Retos</h1>

<div>
      <h3>Selecciona un día</h3>

       <select  name="diaReto" size=1 class="filtroDia">
        <option >Dias</option>
          <option value="Lunes">Lunes</option>
          <option value="Martes">Martes</option>
          <option value="Miercoles">Miercoles</option>
          <option value="Jueves">Jueves</option> 
          <option value="Viernes">Viernes</option>
          <option value="Sabado">Sabado</option>
          <option value="Domingo">Domingo</option>
      </select>
</div>
      <table class="table table-stripped tabla-reto">
        <thead>
        <tr>
          <th>Encargado</th>
          <th>Día</th>
          <th>Hora</th>
          <th>Teléfono</th>
          <th>Acción
          <br></th>
        </tr>
        </thead>
        <tbody>
        <?php
          if($fetch_data->num_rows()>0){
            foreach ($fetch_data->result() as $row) 
            {
          ?>
            <tr> 
              <td><?php echo $row->encargado; ?></td>
              <td><?php echo $row->diaReto; ?></td>
              <td><?php echo $row->horaReto; ?></td>
              <td><?php echo $row->telefono; ?></td>
              <td>
                <a href="<?php echo base_url();?>RegistroReto/update_data/<?php echo $row->idEquiposReto;?> "><img src="<?php echo base_url(); ?>/files/img/mod.png"></a>
              <a href="#" class="eliminar"id="<?php echo $row->idEquiposReto ?>""><img src="<?php echo base_url(); ?>"/files/img/eli.png"></a>
            </td>
            </tr> 
          <?php
              
            }

          }
          else
          {
          ?>
            <tr> 
                <td colsdan="3"> No hay información para mostrar</td>
            </tr>
          <?php  
          }
          ?>

        </tbody>
        
      </table>
      <div class="botones-equipos-container">
       <a href="<?php echo base_url(); ?>principal" class="btn btn-salir-standard">Volver a Principal</a>  
        <a href="<?php echo base_url(); ?>registroReto" class="btn btn-agregar">Añadir</a>
             </div>
    </div> 

     <footer>
       <div class="leyenda-footer">
           <p>Complejo Deportivo Ronyes </p>
           <p>Ubicados 1km al este de la agencia ICE, Esparza.
           </p>
       </div>
       <div class="icons-container-footer">
           <a href="">
            <img src="<?php echo base_url() ?>/files/img/facebook.png" alt="" class="">
            Siguenos!
            </a>
           <a href="">
            <img src="./img/maps.png" alt="" class="">
            Visitanos!
            </a>            
       </div>
       <p>Desarrollado por: Wildness Developers</p>
   </footer>

    <!-- jQuery -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript REQUERIDO-->
    
    <script src="js/scrolling-nav.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){        
        $('.eliminar').click(function(){
          var id = $(this).attr("id");
          if(confirm("¿Esta seguro que desea eliminarlo?")){
            window.location = "<?php echo base_url() ?>RegistroReto/eliminarReto/"+id;
          }else{
            return false;
          }
        });
        $('.filtroDia').change(function(){          
          var diaSemana = $(this).val();
            $.ajax({
                type:'POST',
                data:{diaSemana: diaSemana},
                url:'<?php echo base_url();?>principalretos/',
                success: function(result){
                  //alert(result);
                $('body').html(result);
                }
             });

        });
      });
    </script>
     

</body>

</html>