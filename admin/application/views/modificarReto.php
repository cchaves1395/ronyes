<!DOCTYPE html>
<html lang="es">
    
     <?php
        if (isset($this->session->userdata['login'])) {
            
        } else {
            redirect("login");
        }
    ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Ronyes, complejo deportivo</title> 

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>/files/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>files/css/style.css" rel="stylesheet">


</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top nav-custom" role="navigation">

        

        <a class="btn btn-agregar btn-volver-principal" href="<?php echo base_url(); ?>principal">
      <span class="glyphicon glyphicon-home"></span>
    </a>  
        <!-- /.container -->
    </nav>
<div class="container registro-content">
    <h1>Modificar equipo para reto</h1>
    <h3>Ingrese los datos del equipo que desea modificar</h3>
    <?php echo form_open('registroReto/actualizarReto'); ?>
  <?php
    foreach ($user_data->result() as $row) 
    {
      ?>  

       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
         <input type="text" name="encargado" value="<?php echo $row->encargado;?>" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" size="50" placeholder="Encargado" readonly>
       
       <select class="comboReto" name="diaReto" size=1>
        <option value="<?php echo $row->diaReto;?>"> <?php echo $row->diaReto;?> </option>
          <option value="Lunes">Lunes</option>
          <option value="Martes">Martes</option>
          <option value="Miercoles">Miercoles</option>
          <option value="Jueves">Jueves</option> 
          <option value="Viernes">Viernes</option>
          <option value="Sabado">Sabado</option>
          <option value="Domingo">Domingo</option>
      </select>

        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 registro-content-campos">
            <input type="number" name="telefono" value="<?php echo $row->telefono;?>" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 campos" size="50" placeholder="Teléfono">
            
            <select class="comboReto" name="horaReto" size=1>
        <option value="<?php echo $row->horaReto;?>"><?php echo $row->horaReto  ?><option>
          <option value="5:00 pm">5:00 pm</option>  
          <option value="6:00 pm">6:00 pm</option>  
          <option value="7:00 pm">7:00 pm</option>  
          <option value="8:00 pm">8:00 pm</option>  
          <option value="9:00 pm">9:00 pm</option>
          <option value="10:00 pm">10:00 pm</option>
      </select>
      </div>

    </center> 
    <input type="hidden" name="idEquiposReto" value="<?php echo $row->idEquiposReto; ?>"> 
    <button href="" class="btn btn-agregar" >Modificar</button>
    <a href="<?php echo base_url(); ?>principalRetos" class="btn btn-salir-standard">Cancelar</a>  

</div>   
<?php
}
?> 
  
  </div>  



     <footer>
       <div class="leyenda-footer">
           <p>Complejo Deportivo Ronyes </p>
           <p>Ubicados 1km al este de la agencia ICE, Esparza.
           </p>
       </div>
       <div class="icons-container-footer">
           <a href="">
            <img src="<?php echo base_url() ?>/files/img/facebook.png" alt="" class="">
            Siguenos!
            </a>
           <a href="">
            <img src="./img/maps.png" alt="" class="">
            Visitanos!
            </a>            
       </div>
       <p>Desarrollado por: Wildness Developers</p>
   </footer>

    <!-- jQuery -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript REQUERIDO-->
    
    <script src="js/scrolling-nav.js"></script>

</body>

</html>