<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class RegistroEquipo extends CI_Controller
{
	
	function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('registroEquipoModel');

    }

    public function index(){
        $this->load->view('registroEquipo');
    }

    public function registrar(){
                        
            $data = array(
                'nombre' => $this->input->post('nombre'),
                'cantIntegrantes' => $this->input->post('cantIntegrantes'),
                'encargado' => $this->input->post('encargado'),
                'contacto' => $this->input->post('contacto')
            );
            
            echo $data['nombre'];

            $result = $this->registroEquipoModel->insert($data);

            if ($result == TRUE) {
                //$data['message_display'] = 'Registration Successfully !';
                redirect('principalEquipos');                
            } else {
                //$data['message_display'] = 'Username already exist!';
                $this->load->view('registroEquipo');
            }
        //}
        
    }
    
    /*$this->form_validation->set_rules('nombre', 'nombre', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email_value', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('registration_form');
        } else {*/
    function update_data(){
        $user_id= $this->uri->segment(3);
        $this->load->model('registroEquipoModel');
        $data['user_data']= $this->registroEquipoModel->fetch_single_data($user_id);
       $data['fetch_data']= $this->registroEquipoModel->fetch_data();
        $this->load->view('modificarEquipo',$data);
    }  

    function actualizarEquipo(){
         $data = array(
                'idEquipo' => $this->input->post('idEquipo'),
                'nombre' => $this->input->post('nombre'),
                'cantIntegrantes' => $this->input->post('cantIntegrantes'),
                'encargado' => $this->input->post('encargado'),
                'contacto' => $this->input->post('contacto')
            );
    $this->db->where('idEquipo',$data["idEquipo"]);
    $this->db->update('equipo',$data);
    redirect("principalEquipos");
}

    function eliminarEquipo(){
        $id = $this->uri->segment(3);
        $this->load->model("registroEquipoModel");
        $this->registroEquipoModel->eliminarEquipo($id);
        redirect("principalEquipos");
    }

}

?>