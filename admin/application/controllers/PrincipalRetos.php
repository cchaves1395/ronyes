<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class PrincipalRetos extends CI_Controller
{
	
	

	public function index(){
		$this->load->model('registroRetoModel');
		if ($this->input->post('diaSemana')!= ''){
		$data['fetch_data'] = $this->registroRetoModel->buscarPorfiltro($this->input->post('diaSemana'));        	
	}else{
        	$data['fetch_data'] = $this->registroRetoModel->fetch_data();
        }

		$this->load->view('principalRetos', $data);
	}

	
}