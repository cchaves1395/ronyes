<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class PrincipalEquipos extends CI_Controller
{
	
	

	public function index(){
		$this->load->model('registroEquipoModel');


		$this->load->helper('url');
	    /* Se cargan el modelo alumno y la libreria pagination */ 
	    $this->load->library('pagination');
	    /* URL a la que se desea agregar la paginación*/
	    $config['base_url'] = base_url().'PrincipalEquipos/index/';


	    /*Obtiene el total de registros a paginar */
	    $config['total_rows'] = $this->registroEquipoModel->get_total_equipos();
	      
	    /*Obtiene el numero de registros a mostrar por pagina */
	    $config['per_page'] = 4;
	 
	    /*Indica que segmento de la URL tiene la paginación, por default es 3*/
	    $config['uri_segment'] = 0;

	    /*Se personaliza la paginación para que se adapte a bootstrap*/
	    $config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['last_link'] = FALSE;
	    $config['first_link'] = FALSE;
	    $config['next_link'] = '&raquo;';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['prev_link'] = '&laquo;';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	  
	    /* Se inicializa la paginacion*/
	    $this->pagination->initialize($config);
	  
	    /* Se obtienen los registros a mostrar*/
	   // $data['equipo'] = $this->principalEquiposModel->get_alumnos($config['per_page'], $config['uri_segment']);  
	      
	   
        $data['fetch_data'] = $this->registroEquipoModel->fetch_data($config['per_page'], $config['uri_segment']);
        $this->load->view('principalEquipos', $data);

		//$this->load->view('principalEquipos');

		
	}

	
}