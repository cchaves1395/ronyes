<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class reservacion extends CI_Controller
{
		

	public function consultarConFecha(){

		$this->load->helper('date');
		//variables de entrada
		$fechaBusqueda = mdate('%d-%m-%Y', strtotime($this->uri->segment(3)));		
		if($this->uri->segment(4) == ''){
			$idCancha ='1';
		}else{
			$idCancha  = $this->uri->segment(4);	
		}


		
		

		$this->load->model("reservacionModel");
		$reservacionesEnDB['reservacionesEnDB']= $this->reservacionModel->fetch_data($idCancha,$fechaBusqueda);
		// cargando helper, formato y array de horas
		
		$formato ='%h:%i %a';
		$arrayHoras = array();

		// agregando el horario de inicio y final de la cancha
		$horaInicial = strtotime(mdate($formato,strtotime('2017-06-18 05:00:00 pm')));
		$horaFinal = strtotime(mdate($formato,strtotime('2017-06-18 10:00:00 pm')));	
		// definiendo intervalos de tiempo en segundos 3600 segundos = 1 hora
		$intervaloHora = 3600;
		$intervaloMinutos = 60;
		
		//creando fecha para buscar
		
		
		
		

		for ($i=$horaInicial; $i <=$horaFinal ; $i+=$intervaloHora) {
			$objReservacion = new stdClass();
			if($reservacionesEnDB['reservacionesEnDB']->num_rows()>0){ // si hay datos en la DB
				
				foreach ($reservacionesEnDB['reservacionesEnDB']->result() as $row) {
					if($row->fecha == $fechaBusqueda && mdate($formato,strtotime($row->hora)) == mdate($formato,$i)){			

					// si encontramos uno que sea de la misma fecha y hora

						$objReservacion = $row;
						break;

					}else{
					$objReservacion->idReservacion='0';							
					$objReservacion->fecha=$fechaBusqueda;			
					$objReservacion->hora= mdate($formato,$i);			
					$objReservacion->idCancha=$idCancha;
					$objReservacion->equipo1='';
					$objReservacion->equipo2='';
					$objReservacion->contacto1='';
					$objReservacion->contacto2='';
					$objReservacion->arbitro='';
					$objReservacion->reto='';
					
					}//cierre del if
					
				}

				$arrayHoras[]= $objReservacion;

			}else{// cuando no hay datos en la DB, llenar con información por defecto (libre todas)

					$objReservacion->idReservacion='0';	
					$objReservacion->fecha=$fechaBusqueda;			
					$objReservacion->hora= mdate($formato,$i);			
					$objReservacion->idCancha=$idCancha;
					$objReservacion->equipo1='';
					$objReservacion->equipo2='';
					$objReservacion->contacto1='';
					$objReservacion->contacto2='';
					$objReservacion->arbitro='';
					$objReservacion->reto='';
					//agregando el objeto
					$arrayHoras[]=$objReservacion;
			}

		}	
		$data ['reservaciones'] = $arrayHoras;
	
		
        $this->load->view('reservacion',$data);

	}
	

	public function index(){

		$this->load->helper('date');
		//variables de entrada
		$fechaBusqueda = mdate('%d-%m-%Y', strtotime($this->uri->segment(3)));
		if($this->uri->segment(4) == ''){
			$idCancha ='1';
		}else{
			$idCancha = $this->uri->segment(4);	
		}
		

		$this->load->model("reservacionModel");
		$reservacionesEnDB['reservacionesEnDB']= $this->reservacionModel->fetch_data($idCancha,$fechaBusqueda);
		// cargando helper, formato y array de horas
		
		$formato ='%h:%i %a';
		$arrayHoras = array();

		// agregando el horario de inicio y final de la cancha
		$horaInicial = strtotime(mdate($formato,strtotime('2017-06-18 05:00:00 pm')));
		$horaFinal = strtotime(mdate($formato,strtotime('2017-06-18 10:00:00 pm')));	
		// definiendo intervalos de tiempo en segundos 3600 segundos = 1 hora
		$intervaloHora = 3600;
		$intervaloMinutos = 60;
		
		//creando fecha para buscar
		$fechaBusqueda = mdate('%d-%m-%Y', time());
		
		
		

		for ($i=$horaInicial; $i <=$horaFinal ; $i+=$intervaloHora) {
			$objReservacion = new stdClass();
			if($reservacionesEnDB['reservacionesEnDB']->num_rows()>0){ // si hay datos en la DB
				
				foreach ($reservacionesEnDB['reservacionesEnDB']->result() as $row) {
					if($row->fecha == $fechaBusqueda && mdate($formato,strtotime($row->hora)) == mdate($formato,$i)){			

					// si encontramos uno que sea de la misma fecha y hora

						$objReservacion = $row;
						break;

					}else{
					$objReservacion->idReservacion='0';	
					$objReservacion->fecha=$fechaBusqueda;			
					$objReservacion->hora= mdate($formato,$i);			
					$objReservacion->idCancha=$idCancha;
					$objReservacion->equipo1='';
					$objReservacion->equipo2='';
					$objReservacion->contacto1='';
					$objReservacion->contacto2='';
					$objReservacion->arbitro='';
					$objReservacion->reto='';
					
					}//cierre del if
					
				}

				$arrayHoras[]= $objReservacion;

			}else{// cuando no hay datos en la DB, llenar con información por defecto (libre todas)
					$objReservacion->idReservacion='0';	
					$objReservacion->fecha=$fechaBusqueda;			
					$objReservacion->hora= mdate($formato,$i);			
					$objReservacion->idCancha=$idCancha;
					$objReservacion->equipo1='';
					$objReservacion->equipo2='';
					$objReservacion->contacto1='';
					$objReservacion->contacto2='';
					$objReservacion->arbitro='';
					$objReservacion->reto='';
					//agregando el objeto
					$arrayHoras[]=$objReservacion;
			}

		}	
		$data ['reservaciones'] = $arrayHoras;
	
		
        $this->load->view('reservacion',$data);

		
	}


	
}