<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Login extends CI_Controller
{
     
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('logeo');

    }
     
    public function index()
    {
        /*$cedula=$this->input->post('cedula');
        $contraseña=$this->input->post('contraseña');
        
        $data = array(
         'cedula' => $cedula,
            'login' => true, 
        );
        
        $this->session->set_userData($data);
        //para sacar el dato de la cookie
        echo $this->session->userdata('cedula');*/
    
        $this->load->view('login');
    }
     
    public function procesar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cedula', 'cedula', 'required|min_length[3]|max_length[12]');
        $this->form_validation->set_rules('contraseña', 'contraseña', 'required|min_length[3]|max_length[12]');
        
        if ($this->form_validation->run() == FALSE) {
            
            $this->load->view('login');
        } else {
            
            $this->load->helper('form');
            $data = array(
                'cedula' => $this->input->post('cedula'),
                'contraseña' => $this->input->post('contraseña')
                );
            
            $result = $this->logeo->login($data);
            if($result == TRUE) {
                
                $datasession = array(
                    'cedula' => $this->input->post('cedula'),
                    'login' => true 
                );
                
                $this->session->set_userData($datasession);
                
                $this->load->view('principal');
            
            } else {
                
                  redirect("login");
            }
        
        }
    }
    
    public function cerrar(){
        
        $datasession = array(
            'login' => false 
        );
        $this->session->set_userData($datasession);
        $this->session->sess_destroy();
        
        redirect("login");
    }
    
    
}