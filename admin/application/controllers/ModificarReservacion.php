<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class ModificarReservacion extends CI_Controller
{
    
	function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('reservacionModel');

    }
	

	public function index(){

        $objReservacion = new stdClass();
        $objReservacion->fecha = $this->input->post('fechaReservacion');        
        $objReservacion->hora = $this->input->post('hora');
        $objReservacion->idCancha = $this->input->post('idCancha');
        $objReservacion->idReservacion = $this->input->post('idReservacion');
        $objReservacion->equipo1 = $this->input->post('equipo1');        
        $objReservacion->equipo2 = $this->input->post('equipo2');
        $objReservacion->contacto1 = $this->input->post('contacto1');
        $objReservacion->contacto2 = $this->input->post('contacto2');
        $objReservacion->reto = $this->input->post('reto');
        $objReservacion->arbitro = $this->input->post('arbitro');

        
        $this->load->view('modificarReservacion',$objReservacion);

	}
    
    
    public function guardar(){
        
        $retoAux;
        $arbitroAux;
        
        if($this->input->post('reto')!=null){
            $retoAux = "si";
        } else {
            $retoAux = "no";
        }
        if($this->input->post('arbitro')!=null){
            $arbitroAux = "si";
        } else {
            $arbitroAux = "no";
        }
       
        $data = array(
                'fecha' => $this->input->post('fecha'),
                'hora' => $this->input->post('hora'),
                'idCancha' => $this->input->post('idCancha'),
                'equipo1' => $this->input->post('equipo1'),
                'contacto1' => $this->input->post('contacto1'),
                'equipo2' => $this->input->post('equipo2'),
                'contacto2' => $this->input->post('contacto2'),
                'reto' => $retoAux,
                'arbitro' => $arbitroAux
            );
            

            $result = $this->reservacionModel->insert($data);

            if ($result == TRUE) {
                redirect('reservacion/consultarConFecha/'.$this->input->post('fecha'). '/'.$this->input->post('idCancha'));                
            } else {
                $this->load->view('modificarReservacion');
            }
        
        
    }//fin de la funcion guardarReservacion


}