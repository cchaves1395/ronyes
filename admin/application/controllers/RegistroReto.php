<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class RegistroReto extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('registroRetoModel');

    }

public function index(){
    $this->load->view('registroReto');
}
    
    public function registrar(){
                        
            $data = array(
                'encargado' => $this->input->post('encargado'),
                'telefono' => $this->input->post('telefono'),
                'diaReto' => $this->input->post('diaReto'),
                'horaReto' => $this->input->post('horaReto')
            );
            
            echo $data['encargado'];

            $result = $this->registroRetoModel->insert($data);

            if ($result == TRUE) {
                //$data['message_display'] = 'Registration Successfully !';
                redirect('principalRetos');                
            } else {
                //$data['message_display'] = 'Username already exist!';
                $this->load->view('registroReto');
            }

        
    }
    
    function update_data(){
        $user_id= $this->uri->segment(3);
        $this->load->model('registroRetoModel');
        $data['user_data']= $this->registroRetoModel->fetch_single_data($user_id);
       $data['fetch_data']= $this->registroRetoModel->fetch_data();
        $this->load->view('modificarReto',$data);
    }  

    function actualizarReto(){
         $data = array(
                'idEquiposReto' => $this->input->post('idEquiposReto'),
                'encargado' => $this->input->post('encargado'),
                'telefono' => $this->input->post('telefono'),
                'diaReto' => $this->input->post('diaReto'),
                'horaReto' => $this->input->post('horaReto')
            );
    $this->db->where('idEquiposReto',$data["idEquiposReto"]);
    $this->db->update('EquiposReto',$data);
    redirect("principalRetos");
}

    function eliminarReto(){
        $id = $this->uri->segment(3);
        $this->load->model("registroRetoModel");
        $this->registroRetoModel->eliminarReto($id);
        redirect("principalRetos");
    }

}

?>