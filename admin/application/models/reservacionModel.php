<?php

Class ReservacionModel extends CI_Model {

    function __construct(){

    }

    public function insert($data) {
        
        $condition = "fecha =" . "'" . $data['fecha'] . "' AND " . " hora = " . "'" . $data['hora']  . "' AND " . " idCancha = " . "'" . $data['idCancha'] . "'";
        $this->db->select('*');
        $this->db->from('reservacion');
        $this->db->where($condition);
        //$this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 0) {
            $this->db->insert('reservacion', $data);
            
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            $this->db->select('idReservacion');
            $this->db->from('reservacion');
            $this->db->where($condition);
            $query = $this->db->get();
            
             if ($query->num_rows() != 0) {
                $this->db->where('fecha',$data['fecha']);
                $this->db->where('hora',$data['hora']);
                $this->db->where('idCancha',$data['idCancha']);
                $this->db->update('reservacion',$data);
             }
            
            return true;
        }
    }
    
    public function fetch_data($idCancha, $fechaBusqueda){
        $condicion = "idCancha=" . "'" . $idCancha . "' && fecha='" . $fechaBusqueda . "'";
        $this->db->where($condicion);
        $query = $this->db->get('reservacion');
        return $query;
    }

    function fetch_single_data($idCancha){
        $this->db->where('idCancha', $idCancha);
        $query= $this->db->get('reservacion');
        return $query;
    }

    function update_data($data, $id){
        $this->db->where('idEquipo',$id);
        $this->db->update('equipo',$data);
    }


    function eliminarEquipo($idCancha){
        $this->db->where("idCancha",$idCancha);
        $this->db->delete("reservacion");
    }


}// fin de la clase



?>