<?php

Class RegistroEquipoModel extends CI_Model {

    function __construct(){

    }

    public function insert($data) {
        
        $condition = "nombre =" . "'" . $data['nombre'] . "'";
        $this->db->select('*');
        $this->db->from('equipo');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 0) {
            $this->db->insert('equipo', $data);
            
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return false;
        }
    }
function fetch_data($porpagina,$segmento)
  {
    $query = $this->db->get('equipo',$porpagina, $this->uri->segment(3) /*segmento*/);
    return $query;
    /*if( $query->num_rows() > 0 )
      return $query->result();
    else
      return FALSE; */
  }
 
  function get_total_equipos(){
    return $this->db->count_all('equipo');
  }

function fetch_single_data($idEquipo){
    $this->db->where('idEquipo', $idEquipo);
    $query= $this->db->get('equipo');
    return $query;
}


function update_data($data, $id){
    $this->db->where('idEquipo',$id);
    $this->db->update('equipo',$data);
}

function eliminarEquipo($id){
    $this->db->where("idEquipo",$id);
    $this->db->delete("equipo");
}


}// fin de la clase



?>