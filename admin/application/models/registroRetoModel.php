<?php

Class RegistroRetoModel extends CI_Model {

    function __construct(){

    }

    public function insert($data) {
        
        $condition = "encargado =" . "'" . $data['encargado'] . "'";
        $this->db->select('*');
        $this->db->from('equiposReto');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 0) {
            $this->db->insert('equiposReto', $data);
            
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return false;
        }
    }
public function fetch_data(){
    $query = $this->db->get('EquiposReto');
    return $query;
}

public function buscarPorfiltro($diaSemana){
    $this->db->where('diaReto',$diaSemana);
    $query = $this->db->get('EquiposReto');
    return $query;
}

function fetch_single_data($idEquiposReto){
    $this->db->where('idEquiposReto', $idEquiposReto);
    $query= $this->db->get('EquiposReto');
    return $query;
}


function update_data($data, $id){
    $this->db->where('idEquiposReto',$id);
    $this->db->update('EquiposReto',$data);
}

function eliminarReto($id){
    $this->db->where("idEquiposReto",$id);
    $this->db->delete("EquiposReto");
}


}// fin de la clase



?>