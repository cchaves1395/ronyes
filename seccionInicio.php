<section id="intro" class="intro-section">
  <div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>Complejo Deportivo RONYES</h1>  
          
            <div class="col-md-10 col-md-offset-1">
                
                <!-- Carousel -->                
                <div id="carousel-inicio" class="carousel slide" data-ride="carousel">
                    
                    <!-- Indicadores -->
                    <ol class="carousel-indicators"> 
                        <li data-target="#carousel-inicio" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-inicio" data-slide-to="1"></li>
                        <li data-target="#carousel-inicio" data-slide-to="2"></li>
                    </ol>
                    
                    <!-- Items -->
                    <div class="carousel-inner" rol="listbox">
                        <div class="item active">
                            <img src="img/ronyes-1.png" class="img-responsive" alt="">
                            <div class="carousel-caption hidden-xs hidden-sm">
                                <h3>Nuestras canchas disponibles</h3>
                            </div>
                        </div>
                        
                        <div class="item">
                            <img src="img/ronyes-2.png" class="img-responsive" alt="">
                            <div class="carousel-caption hidden-xs hidden-sm">
                                <h3>Excelente estado para su uso</h3>
                            </div>
                        </div>
                        
                        <div class="item">
                            <img src="img/ronyes-3.png" class="img-responsive" alt="">
                            <div class="carousel-caption hidden-xs hidden-sm">
                                <h3>Hechas con la mejor calidad</h3>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Controles -->
                    <a href="#carousel-inicio" class="left carousel-control" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a href="#carousel-inicio" class="right carousel-control" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Siguiente</span>
                    </a>
                
                </div>
                
            </div>
            
            <!-- Informacion de textos -->
            <div class="col-med-10 col-md-offset-2">                
                <article class="col-sm-5">
                    <h3>Servicios</h3>
                    <hr class="hidden-xs hidden-sm">
                    <p>Contamos con canchas para futbol 5 y futbol 8;<br class="hidden-xs hidden-sm"> 
                        además de amplio parqueo y servicio de soda.<br class="hidden-xs hidden-sm">
                        Ven y disfruta con mucha comodidad.</p>
                </article>
                <article class="col-sm-5">
                    <h3>Torneos</h3>
                    <hr class="hidden-xs hidden-sm">
                    <p>Compite con tu equipo en grandiosos torneos para 
                        desmostrar tus capacidades contra otros equipos.<br class="hidden-xs hidden-sm">
                        Participa y gana.</p>
                </article>
            </div>
                         
      </div>
    </div>
  </div>
</section>